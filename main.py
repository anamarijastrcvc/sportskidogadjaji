import flask
from flask import Flask
from db import mysql

from blueprints.login import login_blueprint
from blueprints.user import user_blueprint
from blueprints.event import event_blueprint
from blueprints.ticket import ticket_blueprint

app = Flask(__name__, static_url_path="")

app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "sportski_dogadjaji"
app.config["MYSQL_DATABASE_HOST"] = "localhost"
app.config["SECRET_KEY"] = "oHybEgSWREKYG1nzeDpqOw=="

mysql.init_app(app)
app.register_blueprint(login_blueprint)
app.register_blueprint(user_blueprint)
app.register_blueprint(event_blueprint)
app.register_blueprint(ticket_blueprint)

@app.route("/")
@app.route("/index")
@app.route("/index.html")
def home():
    return app.send_static_file("index.html")

app.run("0.0.0.0", 8080, threaded=True)