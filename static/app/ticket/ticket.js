(function(angular){
    var app = angular.module("app");
    app.controller("TicketController", ["$http", function($http) {
        var tickets = this;
        tickets.userType = localStorage.getItem("userType");

        tickets.getTickets = function() {
            if(tickets.userType == "user") {
                $http.get("/user-tickets").then(function(response) {
                    tickets.tickets = response.data
                }, function(reason) {
                    console.log(reason);
                });
            } else if(tickets.userType == "admin") {
                $http.get("/tickets").then(function(response) {
                    tickets.tickets = response.data
                }, function(reason) {
                    console.log(reason);
                });
            }
        }
        
        tickets.add = function() {
            $http.post("/ticket", tickets.newTicket).then(function(response){
                if(response.data["status"] == "success") {
                    tickets.getTickets();
                }
            },
            function(reason){
                console.log(reason);
            })
        };

        tickets.delete = function(id) {
            $http.delete("/ticket/"+id).then(function(response){
                tickets.getTickets();
            },
            function(reason){
                console.log(reason)
            });
        };
        
        tickets.prepForEdit = function(ticket) {
            tickets.editTicket = angular.copy(ticket);
        }

        tickets.cancelEdit = function() {
            tickets.editTicket = {};
        }

        tickets.edit = function() {
            $http.put("/ticket/"+tickets.editTicket.id, tickets.editTicket).then(function(response){
                tickets.getTickets();
                tickets.editTicket = {};
            },
            function(reason){
                console.log(reason)
            });
        }

        tickets.getTickets();
    }]);
})(angular);