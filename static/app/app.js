
(function(angular) {
    var app = angular.module('app',["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        $stateProvider.state({
            name: "login",
            url: "/login",
            templateUrl: "app/login/login.html",
            controller: "LoginController",
            controllerAs: "ctrl"
        }).state({
            name: "register",
            url: "/register",
            templateUrl: "app/register/register.html",
            controller: "RegisterController",
            controllerAs: "ctrl"
        }).state({
            name: "user",
            url: "/user",
            templateUrl: "app/user/user.html",
            controller: "UserController",
            controllerAs: "ctrl"
        }).state({
            name: "event",
            url: "/event",
            templateUrl: "app/event/event.html",
            controller: "EventController",
            controllerAs: "ctrl"
        }).state({
            name: "ticket",
            url: "/ticket",
            templateUrl: "app/ticket/ticket.html",
            controller: "TicketController",
            controllerAs: "ctrl"
        });

        
        $urlRouterProvider.otherwise("/login")
    }]);
    app.run(function ($rootScope, $location) {
        $rootScope.location = $location;
    });
})(angular);