(function(angular){
    var app = angular.module("app");
    app.controller("EventController", ["$http", function($http) {
        var events = this;
        events.editEventInProgress = false;
        events.userType = localStorage.getItem("userType");
        
        events.getEvents = function() {
            $http.get("/events").then(function(response) {
                events.events = response.data
            }, function(reason) {
                console.log(reason);
            });
        }
        
        events.add = function() {
            events.newEvent.id = events.events.length + 1;
            $http.post("/event", events.newEvent).then(function(response){
                if(response.data["status"] == "success") {
                    events.getEvents();
                }
            },
            function(reason){
                console.log(reason);
            })
        };

        events.buyTicket = function(ticket) {
            $http.post("/ticket", ticket).then(function(response){
                $state.go("tickets");
            },
            function(reason){
                console.log(reason);
            })
        };


        events.delete = function(id) {
            $http.delete("/event/"+id).then(function(response){
                events.getEvents();
            },
            function(reason){
                console.log(reason)
            });
        };
        
        events.prepForEdit = function(event) {
            events.editEventInProgress = true;
            events.editEvent = angular.copy(event);
        }

        events.cancel = function() {
            events.editEvent = {};
            events.editEventInProgress = false;
        }

        events.edit = function() {
            $http.put("/event", events.editEvent).then(function(response){
                events.getEvents();
                events.editEvent = {};
                events.editEventInProgress = false;
            },
            function(reason){
                console.log(reason)
            });
        }

        events.getEvents();
    }]);
})(angular);