(function(angular){
    var app = angular.module("app");
    app.controller("RegisterController", ["$http", "$state", function($http, $state) {
        var register = this;

        register.add = function() {
            $http.post("/user", register.newUser).then(function(response){
                if(response.data["status"] == "success") {
                    $state.go("login")
                }
            },
            function(reason){
                console.log(reason);
            })
        };
    }]);
})(angular);