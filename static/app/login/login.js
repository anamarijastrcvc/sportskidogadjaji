(function(angular){
    var app = angular.module("app");
    app.controller("LoginController", ["$http", "$state", "$scope", "$rootScope", function($http, $state, $scope, $rootScope) {
        var login = this;
        this.userType = localStorage.getItem("userType");

        $scope.$on("loggedIn", function() {
            login.userType = localStorage.getItem("userType");
        })

        this.login = function() {
            $http.post("/login", login.loginUser).then((res) => {
                login.loggedIn = true;
                login.getUser().then((response) => {
                    var user = response.data;
                    var state;
                    if(user.type == "admin") {
                        localStorage.setItem("userType", "admin");
                        state = "user"
                    } else if(user.type == "user") {
                        localStorage.setItem("userType", "user");
                        state = "event"
                    }
                    $rootScope.$broadcast("loggedIn", $scope.name)
                    $state.go(state)
                }, (reason) => {
                    alert("Neispravni kredencijali.");
                })
            }, (reason) => {
                alert("Neispravni kredencijali.");
            } )
        }

        this.getUser = function() {
            return $http.get("/logged-in")
        }

        this.logout = function() {
            login.userType = localStorage.setItem("userType", "");
            return $http.get("/logout").then();
        }
    }]);
})(angular);