(function(angular){
    var app = angular.module("app");
    app.controller("UserController", ["$http", function($http) {
        var users = this;
        users.userType = localStorage.getItem("userType");
        
        users.getUsers = function() {
            $http.get("/users").then(function(response) {
                users.users = response.data
            }, function(reason) {
                console.log(reason);
            });
        }
        
        users.add = function() {
            users.newUser.id = users.users.length + 1;
            $http.post("/user", users.newUser).then(function(response){
                if(response.data["status"] == "success") {
                    users.getUsers();
                }
            },
            function(reason){
                console.log(reason);
            })
        };

        users.delete = function(id) {
            $http.delete("/user/"+id).then(function(response){
                users.getUsers();
            },
            function(reason){
                console.log(reason)
            });
        };
        
        users.prepForEdit = function(user) {
            users.editUser = angular.copy(user);
        }

        users.cancelEdit = function() {
            users.editUser = {};
        }

        users.edit = function() {
            $http.put("/user", users.editUser).then(function(response){
                users.getUsers();
                users.editUser = {};
            },
            function(reason){
                console.log(reason)
            });
        }
        $http.get("/logged-in").then(function(response) {
            users.editUser = response.data;
        })
        users.getUsers();
    }]);
})(angular);