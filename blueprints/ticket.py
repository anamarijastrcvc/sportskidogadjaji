import flask
from flask import Blueprint

from db import mysql

ticket_blueprint = Blueprint("ticket_blueprint", __name__)

@ticket_blueprint.route("/ticket", methods=["POST"])
def add_ticket():
    user = flask.session["user"]
    if user == None: 
         return  flask.jsonify({"status": "failure"}), 401
    try:
        data = flask.request.json
        cursor = mysql.get_db().cursor()
        query = "INSERT INTO ticket(idticket, description, price, event_id, user_id) VALUES(%s, %s, %s, %s, %s, %s)"
        cursor.execute(query, (data["idticket"], data["description"], data["price"], data["event_id"], data["user_id"]))
        mysql.get_db().commit()
        return flask.jsonify({"status": "success"}), 200
    except Exception as e:
        return flask.jsonify({"status": "failure"}), 400

@ticket_blueprint.route("/ticket", methods=["PUT"])
def edit():
    user = flask.session["user"]
    if user == None or user["type"] == "user": 
         return  flask.jsonify({"status": "failure"}), 401

    try:
        data = flask.request.json
        cursor = mysql.get_db().cursor()
        query = "UPDATE ticket SET description=%s, price=%s, event_id=%s, user_id=%s WHERE idticket=%s";
        cursor.execute(query, (data["description"], data["price"], data["event_id"], data["user_id"], data["idticket"]))
        mysql.get_db().commit()
        return flask.jsonify({"status": "success"}), 200
    except Exception as e:
        return flask.jsonify({"status": "failure"}), 400
   

@ticket_blueprint.route("/tickets", methods=["GET"])
def get_tickets():
    user = flask.session["user"]
    if user == None: 
         return  flask.jsonify({"status": "failure"}), 401
    data = flask.request.json
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM ticket")
    ticket = cursor.fetchall()
    return flask.jsonify(ticket), 200


@ticket_blueprint.route("/ticket", methods=["GET"])
def get_ticket():
    user = flask.session["user"]
    if user == None: 
         return  flask.jsonify({"status": "failure"}), 401
    data = flask.request.json
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM ticket WHERE idticket=%s", (data["idticket"]))
    ticket = cursor.fetchone()
    return flask.jsonify(ticket), 200

@ticket_blueprint.route("/user-tickets", methods=["GET"])
def get_ticket_for_user():
    user = flask.session["user"]
    if user == None: 
         return  flask.jsonify({"status": "failure"}), 401
    data = flask.request.json
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM ticket WHERE user_id =%s", (data["idticket"], user["id"]))
    ticket = cursor.fetchall()
    return flask.jsonify(ticket), 200

@ticket_blueprint.route("/event-tickets", methods=["GET"])
def get_ticket_for_event():
    user = flask.session["user"]
    if user == None or user["type"] == "user": 
         return  flask.jsonify({"status": "failure"}), 401

    data = flask.request.json
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM ticket WHERE idticket=%s and event_id =%s", (data["idticket"], data["event_id"]))
    ticket = cursor.fetchall()
    return flask.jsonify(ticket), 200