import flask
from flask import Blueprint

from db import mysql

user_blueprint = Blueprint("user_blueprint", __name__)

@user_blueprint.route("/user", methods=["POST"])
def register():
    try:
        data = flask.request.json
        data["type"] = "user"
        cursor = mysql.get_db().cursor()
        cursor.execute("SELECT count(*) FROM user")
        count = cursor.fetchone()
        count = count["count(*)"] + 1;
        query = "INSERT INTO user(id, username, password, firstname, lastname, type) VALUES(%s, %s, %s, %s, %s, %s)"
        cursor.execute(query, (count, data["username"], data["password"], data["firstname"], data["lastname"], data["type"]))
        mysql.get_db().commit()
        return flask.jsonify({"status": "success"}), 200
    except Exception as e:
        return flask.jsonify({"status": "failure"}), 400

@user_blueprint.route("/user", methods=["PUT"])
def edit():
    user = flask.session["user"]
    if user == None: 
         return  flask.jsonify({"status": "failure"}), 401

    try:
        data = flask.request.json
        cursor = mysql.get_db().cursor()
        query = "UPDATE user SET username=%s, password=%s, firstname=%s, lastname=%s WHERE id=%s";
        cursor.execute(query, (data["username"], data["password"], data["firstname"], data["lastname"], int(data["id"])))
        mysql.get_db().commit()
        return flask.jsonify({"status": "success"}), 200
    except Exception as e:
        return flask.jsonify({"status": "failure"}), 400
   

@user_blueprint.route("/users", methods=["GET"])
def get_users():
    user = flask.session["user"]
    if user == None or user["type"] != "admin": 
         return  flask.jsonify({"status": "failure"}), 401

    data = flask.request.json
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM user")
    user = cursor.fetchall()
    return flask.jsonify(user), 200


@user_blueprint.route("/user", methods=["GET"])
def get_user():
    user = flask.session["user"]
    if user == None or user["type"] == "user": 
         return  flask.jsonify({"status": "failure"}), 401

    data = flask.request.json
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM user WHERE username=%s", (data["username"]))
    user = cursor.fetchone()
    return flask.jsonify(user), 200