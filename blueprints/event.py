import flask
from flask import Blueprint

from db import mysql

event_blueprint = Blueprint("event_blueprint", __name__)

@event_blueprint.route("/event", methods=["POST"])
def add_event():
    user = flask.session["user"]
    if user == None or user["type"] == "user": 
         return  flask.jsonify({"status": "failure"}), 401

    try:
        data = flask.request.json
        cursor = mysql.get_db().cursor()
        query = "INSERT INTO event(id, name, description, location, date, city) VALUES(%s, %s, %s, %s, %s, %s)"
        cursor.execute(query, ( int(data["id"]), data["name"], data["description"], data["location"], data["date"], data["city"]))
        mysql.get_db().commit()
        return  flask.jsonify({"status": "success"}), 200
    except Exception as e:
        return  flask.jsonify({"status": "failure"}), 400

@event_blueprint.route("/event", methods=["PUT"])
def edit_event():
    user = flask.session["user"]
    if user == None or user["type"] == "user": 
         return  flask.jsonify({"status": "failure"}), 401

    try:
        data = flask.request.json
        cursor = mysql.get_db().cursor()
        query = "UPDATE event SET name=%s, description=%s, location=%s, date=%s, city=%s WHERE id=%s";
        cursor.execute(query, (data["name"], data["description"], data["location"], data["date"], data["city"], int(data["id"])))
        mysql.get_db().commit()
        return  flask.jsonify({"status": "success"}), 200
    except Exception as e:
        return  flask.jsonify({"status": "failure"}), 400
   
@event_blueprint.route("/events", methods=["GET"])
def get_events():
    data = flask.request.json
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM event")
    event = cursor.fetchall()
    return flask.jsonify(event), 200

@event_blueprint.route("/event", methods=["GET"])
def get_event():
    data = flask.request.json
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM event WHERE name=%s", (data["name"]))
    event = cursor.fetchone()
    return flask.jsonify(event), 200