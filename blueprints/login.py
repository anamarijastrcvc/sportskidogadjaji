import flask
from flask import Blueprint
from db import mysql

login_blueprint = Blueprint("login_blueprint", __name__)

@login_blueprint.route("/login", methods=["POST"])
def login():
    data = flask.request.json
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM user WHERE username=%s AND password=%s", (data["username"], data["password"]))
    user = cursor.fetchone()
    if user is not None:
        flask.session["user"] = user 
        return flask.jsonify({"status": "success"}), 200
    else:
        return flask.jsonify({"status": "failure"}), 404

@login_blueprint.route("/logout", methods=["GET"])
def logout():
    flask.session.pop("user", None) 
    return flask.jsonify({"status": "success"}), 200

@login_blueprint.route("/logged-in", methods=["GET"])
def current_user():
    return flask.jsonify(flask.session.get("user")), 200